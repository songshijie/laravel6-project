<?php
/*
 * Author: top-songshijie
 * Email :997031758@qq.com
 * DateTime: 2020/6/2 17:36
*/

namespace App\Admin\Actions;

use App\Admin\Extensions\Imports\ExampleImports;
use Illuminate\Http\Request;
use Encore\Admin\Actions\Action;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class ImportAction extends Action
{
    public $name = '导入数据';

    protected $selector = '.import-post';

    public function handle(Request $request)
    {
        Excel::import(new ExampleImports, $request->file('file'));

        return $this->response()->success('导入完成！')->refresh();
    }

    public function form()
    {
        $this->file('file', '请选择文件');
    }

    public function html()
    {
        return <<<HTML
        <a class="btn btn-sm btn-default import-post"><i class="fa fa-upload"></i>导入数据</a>
HTML;
    }
}
