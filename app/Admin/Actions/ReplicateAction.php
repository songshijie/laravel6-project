<?php

namespace App\Admin\Actions;

use Illuminate\Http\Request;
use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;

/**
 * 参考链接：https://laravel-admin.org/docs/zh/model-grid-custom-actions
 */
class ReplicateAction extends RowAction
{
    public $name = '复制';

    public function handle(Model $model)
    {

        $model->replicate()->save();

        return $this->response()->success('复制成功!')->refresh();
    }


    public function dialog()
    {
        $this->confirm('确定复制？');
    }

}
