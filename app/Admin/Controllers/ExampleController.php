<?php

namespace App\Admin\Controllers;


use App\Admin\Actions\ImportAction;
use App\Models\Example;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Admin\Actions\ReplicateAction;
use Encore\Admin\Controllers\AdminController;
use App\Admin\Extensions\Exports\ExampleExport;

class ExampleController extends AdminController
{

    protected $title = '示例';


    protected function grid()
    {
        $grid = new Grid(new Example);

        $grid->column('id', 'ID')->sortable();
        $grid->column('title', '标题');
        $grid->column('image', '单图')->gallery(['zooming' => true, 'height' => 80]);
        $grid->column('created_at', '创建时间');
        $grid->column('updated_at', '修改时间');

        $grid->actions(function ($actions) {
            // 给每行添加自定义按钮
            $actions->add(new ReplicateAction);
        });

        // 数据导出
        $grid->exporter(new ExampleExport());

        $grid->tools(function (Grid\Tools $tools) {
            // 数据导入
            $tools->append(new ImportAction());
        });

        return $grid;
    }

    protected function detail($id)
    {
        $show = new Show(Example::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    protected function form()
    {
        $form = new Form(new Example);

        $form->tab('基础信息', function ($form) {

            $form->example('title', '标题');

        })->tab('图片相关', function ($form) {

            $form->image('image', '图片');
            $form->multipleImage('images', '多图');

        })->tab('文本编辑', function ($form) {
            $form->UEditor('content', '内容');
        });

        return $form;
    }
}
