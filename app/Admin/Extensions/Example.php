<?php
/*
 * Author: top-songshijie
 * Email :997031758@qq.com
 * DateTime: 2020/5/22 18:05
*/

namespace App\Admin\Extensions;

use Encore\Admin\Form\Field;

/**
 * 参考链接：https://laravel-admin.org/docs/zh/model-form-field-management
 */
class Example extends Field
{
    protected $view = 'admin.example';

    protected static $css = [

    ];

    protected static $js = [

    ];

    public function render()
    {
        $name = $this->formatName($this->column);

        return parent::render();
    }

}
