<?php
/*
 * Author: top-songshijie
 * Email :997031758@qq.com
 * DateTime: 2020/06/02 16:21
*/

namespace App\Admin\Extensions\Exports;

use Maatwebsite\Excel\Concerns\WithMapping;
use Encore\Admin\Grid\Exporters\ExcelExporter;

/**
 * 参考链接：https://github.com/Maatwebsite/Laravel-Excel
 */
class ExampleExport extends ExcelExporter implements WithMapping
{

    protected $fileName = '文章列表.xlsx';

    protected $headings = ['ID', '标题'];

    protected $columns = [
        'id'      => 'ID',
        'title'   => '标题',
    ];

    public function map($data) : array
    {
        return [
            $data->id,
            '自定义内容',
        ];
    }

}
