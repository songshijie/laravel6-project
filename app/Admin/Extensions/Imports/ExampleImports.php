<?php
/*
 * Author: top-songshijie
 * Email :997031758@qq.com
 * DateTime: 2020/6/3 11:25
*/

namespace App\Admin\Extensions\Imports;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\ToCollection;

class ExampleImports implements ToCollection
{
    public function collection(Collection $rows)
    {
        // 获取到 excel 表格数据，自定义处理
       Log::debug($rows);
    }
}
