<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('admin.home');

    // example
    $router->get('example', 'ExampleController@index');
    $router->get('example/create', 'ExampleController@create');
    $router->get('example/{id}/edit', 'ExampleController@edit');
    $router->post('example', 'ExampleController@store');
    $router->put('example/{id}', 'ExampleController@update');
    $router->delete('example/{id}', 'ExampleController@destroy');

});
