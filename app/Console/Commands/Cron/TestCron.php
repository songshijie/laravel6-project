<?php

namespace App\Console\Commands\Cron;

use Illuminate\Console\Command;

/**
 * 注册到 linux
 * * * * * * php /www/wwwroot/laravel6-project/artisan schedule:run >> /www/wwwroot/laravel6-project/storage/logs/laravel.log 2>&1
 * 2 > &1 代表将错误输出也重定向到普通输出，即也输出到 cron.log 文件。
 */
class TestCron extends Command
{
    protected $signature = 'cron:test';

    protected $description = '定时任务测试';


    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        dd('Hello World!');
    }
}
