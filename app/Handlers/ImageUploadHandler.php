<?php
/*
 * Author: top-songshijie
 * Email :997031758@qq.com
 * DateTime: 2020/05/20 17:21
*/

namespace App\Handlers;

use Illuminate\Support\Str;
use Image;
use Qiniu\Auth;
use Qiniu\Storage\UploadManager;

class ImageUploadHandler
{
    
    // 只允许以下后缀名的图片文件上传
    protected $allowed_ext = ["png", "jpg", "gif", 'jpeg'];

    public function save($file, $folder, $file_prefix, $max_width = false, $is_qiniu = false)
    {
        //这里添加了七牛云的判断----$is_qiniu = false默认是false不启用,要启用的话传入true就可以了
        if($is_qiniu){
            $domain = env('QINIU_DOMAIN');
            $qiniu = self::qiniuUpload($file);
            return [
                'path' => $qiniu,
                'preview_path' => 'http://'.$domain.'/'.$qiniu
            ];
        }
        // 构建存储的文件夹规则，值如：uploads/images/avatars/201709/21/
        // 文件夹切割能让查找效率更高。
        $folder_name = "uploads/images/$folder/" . date("Ym/d", time());

        // 文件具体存储的物理路径，`public_path()` 获取的是 `public` 文件夹的物理路径。
        // 值如：/home/vagrant/Code/larabbs/public/uploads/images/avatars/201709/21/
        $upload_path = public_path() . '/' . $folder_name;

        // 获取文件的后缀名，因图片从剪贴板里黏贴时后缀名为空，所以此处确保后缀一直存在
        $extension = strtolower($file->getClientOriginalExtension()) ?: 'png';

        // 拼接文件名，加前缀是为了增加辨析度，前缀可以是相关数据模型的 ID
        // 值如：1_1493521050_7BVc9v9ujP.png
        $filename = $file_prefix . '_' . time() . '_' . Str::random(10) . '.' . $extension;

        // 如果上传的不是图片将终止操作
        if ( ! in_array($extension, $this->allowed_ext)) {
            return false;
        }

        // 将图片移动到我们的目标存储路径中
        $file->move($upload_path, $filename);

        // 如果限制了图片宽度，就进行裁剪
        if ($max_width && $extension != 'gif') {

            // 此类中封装的函数，用于裁剪图片
            $this->reduceSize($upload_path . '/' . $filename, $max_width);
        }

        return [
            'path' => "/$folder_name/$filename",
            'preview_path' => config('app.url') . "/$folder_name/$filename"
        ];
    }

    public function reduceSize($file_path, $max_width)
    {
        // 先实例化，传参是文件的磁盘物理路径
        $image = Image::make($file_path);

        // 进行大小调整的操作
        $image->resize($max_width, null, function ($constraint) {

            // 设定宽度是 $max_width，高度等比例双方缩放
            $constraint->aspectRatio();

            // 防止裁图时图片尺寸变大
            $constraint->upsize();
        });

        // 对图片修改后进行保存
        $image->save();
    }

    /*
     * 七牛上传图片
     */
    public static function qiniuUpload($file)
    {

        $ak = env('QINIU_AK');
        $sk = env('QINIU_SK');
        $buckey = env('QINIU_BUCKEY');

        $ext = $file->getClientOriginalExtension();
        $auth = new Auth($ak,$sk);
        //生成上传图片的token
        $token = $auth->uploadToken($buckey);
        $key =time().rand(0,9999).".".$ext;
        $uploadMgr = new UploadManager();
        list($ret,$err) = $uploadMgr->putFile($token,$key,$file);
        if($ret){
            return $key;
        }else{
            return null;
        }
    }
}
