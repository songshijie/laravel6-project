<?php
/*
 * Author: top-songshijie
 * Email :997031758@qq.com
 * DateTime: 2020/5/20 15:26
*/

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController
{
    protected function success($data = null, $msg = 'success', $code = 1)
    {
        return response()->json([
            'code' => $code,
            'msg'  => $msg,
            'data' => $data
        ]);
    }

    protected function error($msg = 'error', $data = null, $code = 0)
    {
        return response()->json([
            'code' => $code,
            'msg'  => $msg,
            'data' => $data
        ]);
    }

    protected function user()
    {
        return auth('api')->user();
    }
}
