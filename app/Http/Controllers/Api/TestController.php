<?php
/*
 * Author: top-songshijie
 * Email :997031758@qq.com
 * DateTime: 2020/5/20 15:27
*/

namespace App\Http\Controllers\Api;

use App\Notifications\TestNotification;
use Illuminate\Support\Facades\Notification;
use Mpdf\Mpdf;
use App\Models\User;
use App\Jobs\TestJob;
use App\Events\TestEvent;
use Endroid\QrCode\QrCode;
use Illuminate\Http\Request;
use App\Handlers\ImageUploadHandler;
use Illuminate\Support\Facades\Mail;
use Illuminate\Notifications\Notifiable;

class TestController extends Controller
{
    use Notifiable;

    public function test()
    {

//        $data = $this->es();
//
//        return $this->success($data);
        dd($this->QuickSort([4, 21, 41, 2, 53, 1, 213, 31, 21, 423]));
    }

    function QuickSort(array $container)
    {
        $count = count($container);
        if ($count <= 1) { // 基线条件为空或者只包含一个元素，只需要原样返回数组
            return $container;
        }
        $pivot = $container[0]; // 基准值 pivot
        $left  = $right = [];

        for ($i = 1; $i < $count; $i++) {
            if ($container[$i] < $pivot) {
                $left[] = $container[$i];
            } else {
                $right[] = $container[$i];
            }
        }
        echo "1";
        $left  = $this->QuickSort($left);
        echo "2";
        $right = $this->QuickSort($right);
        echo "3";
        return array_merge($left, [$container[0]], $right);
    }

    /**
     * Elasticsearch 搜索
     */
    public function es()
    {
        return app('es')->get(['index' => 'example', 'id' => 4]);
    }

    /**
     * 消息系统的发送邮件
     */
    public function notificationByEmail()
    {
        Notification::send(User::first(), new TestNotification('notification 的测试数据'));
    }

    /**
     * 事件监听系统
     * 如果监听器继承了 ShouldQueue 则需开启队列才能正常运行,也就是异步运行
     * 开启 redis 队列：php artisan queue:work
     */
    public function event()
    {
        event(new TestEvent('event 测试数据'));
    }

    /**
     * 任务分发
     * 开启 redis 队列：php artisan queue:work
     * 或者使用 Horizon：php artisan horizon
     */
    public function dispatchTest()
    {
        $this->dispatch(new TestJob('这是 delay 的测试参数',3));
    }

    /**
     * 发送邮件
     */
    public function email()
    {
        $data = 'test';
        Mail::send('test.email', ['data' => $data], function ($message){
                $message->to('997031758@qq.com')->subject('邮件标题');
                $message->attach(public_path() . '/tmp/2.pdf');
            }
        );
    }

    /**
     * 将 html 转换成 pdf
     */
    public function pdf()
    {

        $mpdf = new Mpdf(['tempDir' => public_path() . '/tmp']);
        $mpdf->WriteHTML('<h1>Hello world!</h1>');
        $mpdf->Output(public_path() . '/tmp/2.pdf','f');
    }

    /**
     * jwt 授权
     * Authorization: Bearer {token}
     */
    protected function login()
    {
        //return auth('api')->logout(); //删除当前的授权凭证
        //auth('api')->refresh(); //替换当前的授权凭证

        return auth('api')->login(User::first());
    }

    /**
     * 上传图片(支持七牛云)
     */
    protected function upload()
    {
        $request = new Request();
        $upload = new ImageUploadHandler();

        return $upload->save($request->file('image'),'images','user_id',false,false);
    }

    /**
     * 生成二维码
     * 参考网址：https://packagist.org/packages/endroid/qr-code
     */
    protected function qrCode()
    {
        $qrCode = new QrCode('Life is too short to be generating QR codes');

        header('Content-Type: ' . $qrCode->getContentType());
        echo $qrCode->writeString();
    }

    /**
     * 支付
     * 参考网址：https://packagist.org/packages/yansongda/pay
     */
    protected function pay()
    {
        $pay = app('wechat_pay')->miniapp([
            'out_trade_no' => '123456',
            'total_fee'    => 1,
            'body'         => '备注',
            'openid'       => 'openid'
        ]);

        return $pay;
    }

}
