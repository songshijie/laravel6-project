<?php

namespace App\Listeners;

use App\Events\TestEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class TestListener
{

    public function handle(TestEvent $event)
    {
        $data =$event->getData();

        Log::debug('event 测试内容:'.$data);
    }
}
