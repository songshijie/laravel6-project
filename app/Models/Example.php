<?php
/*
 * Author: top-songshijie
 * Email :997031758@qq.com
 * DateTime: 2020/5/20 14:46
*/

namespace App\Models;

use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Model;

class Example extends Model
{
    protected $table = 'example';

    protected $fillable = [
        'title','content','image','images'
    ];

    public function toESArray()
    {
        $arr = Arr::only($this->toArray(), [
            'id',
            'title'
        ]);
        return $arr;
    }

}
