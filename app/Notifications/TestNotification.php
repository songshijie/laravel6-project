<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

/**
 * 可以继承 ShouldQueue 变成异步通知
 */
class TestNotification extends Notification
{
    use Queueable;

    protected  $data;

    public function __construct($data)
    {
        $this->data = $data;
    }


    public function via($notifiable)
    {
        return ['mail'];
    }


    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('邮件标题')
            ->greeting('欢迎词')
            ->line($this->data)
            ->action('查看订单', 'http://www.baidu.com')
            ->success();
    }


    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
