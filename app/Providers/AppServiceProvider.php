<?php

namespace App\Providers;


use Yansongda\Pay\Pay;
use Overtrue\LaravelWeChat\Facade;
use Illuminate\Support\ServiceProvider;
use Elasticsearch\ClientBuilder as ESClientBuilder;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('mini_wechat', function () {
            return Facade::miniProgram();
        });

        $this->app->singleton('wechat_pay', function () {
            return Pay::wechat(config('pay.wechat'));
        });

        $this->app->singleton('es', function () {
            $builder = ESClientBuilder::create()->setHosts(config('database.elasticsearch.hosts'));
            // 配置日志，生产环境可以注释掉
            $builder->setLogger(app('log')->driver());

            return $builder->build();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
