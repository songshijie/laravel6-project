<?php

return [
    'wechat' => [
        'appid'           => env('WECHAT_MINI_PROGRAM_APPID',''),
        'app_id'          => env('WECHAT_MINI_PROGRAM_APPID',''),
        'miniapp_id'      => env('WECHAT_MINI_PROGRAM_APPID',''),
        'mch_id'      => env('WECHAT_MCHID',''),
        'key'         =>  env('WECHAT_KEY',''),
        'notify_url' => '',
        'log'         => [
            'file' => storage_path('logs/wechat_pay.log'),
        ],
    ],
];
